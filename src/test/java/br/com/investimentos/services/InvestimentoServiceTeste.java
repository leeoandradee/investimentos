package br.com.investimentos.services;

import br.com.investimentos.models.InvestimentoModel;
import br.com.investimentos.repositories.InvestimentoRepository;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class InvestimentoServiceTeste {

    @MockBean
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private InvestimentoService investimentoService;

    private InvestimentoModel investimentoMock;

    @BeforeEach
    private void setUp() {
        investimentoMock = new InvestimentoModel();
        investimentoMock.setNome("Poupança");
        investimentoMock.setRendimentoMes(0.2);

    }

    @Test
    public void testarCriarInvestimento() {

        Mockito.when(investimentoRepository.save(Mockito.any(InvestimentoModel.class))).thenReturn(investimentoMock);

        InvestimentoModel investimentoModelRetorno = investimentoService.criarInvestimento(investimentoMock);

        Assertions.assertEquals("Poupança", investimentoModelRetorno.getNome());
        Assertions.assertEquals(0.2 , investimentoModelRetorno.getRendimentoMes());
    }

    @Test
    public void testabuscarTodosInvestimentos() {

        List<InvestimentoModel> investimentosMock = new ArrayList<>();
        investimentosMock.add(investimentoMock);

        Mockito.when(investimentoRepository.findAll()).thenReturn(investimentosMock);

        List<InvestimentoModel> investimentosRetorno = investimentoService.buscarTodosInvestimentos();

        Assertions.assertArrayEquals(investimentosRetorno.toArray(), investimentosMock.toArray());
    }

    @Test
    public void testarBuscarInvestimentoPorId() {

        Optional<InvestimentoModel> investimentoOptionalMock = Optional.of(investimentoMock);

        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(investimentoOptionalMock);

        InvestimentoModel investimentoModelRetorno = investimentoService.buscarInvestimentoPorId(1);

        Assertions.assertEquals(investimentoMock, investimentoModelRetorno);

    }


}
