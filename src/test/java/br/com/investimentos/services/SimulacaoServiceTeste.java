package br.com.investimentos.services;

import br.com.investimentos.DTOs.ResponseSimulacaoDTO;
import br.com.investimentos.models.InvestimentoModel;
import br.com.investimentos.models.SimulacaoModel;
import br.com.investimentos.repositories.InvestimentoRepository;
import br.com.investimentos.repositories.SimulacaoRepository;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class SimulacaoServiceTeste {

    @MockBean
    private SimulacaoRepository simulacaoRepository;

    @MockBean
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private SimulacaoService simulacaoService;

    @Autowired
    private InvestimentoService investimentoService;

    private SimulacaoModel simulacaoMock;

    private InvestimentoModel investimentoMock;

    @BeforeEach
    public void setUp() {

        simulacaoMock = new SimulacaoModel();
        simulacaoMock.setId(1);
        simulacaoMock.setNomeInteressado("Leonardo");
        simulacaoMock.setValorAplicado(5000.0);
        simulacaoMock.setQuantidadeMeses(12);

        investimentoMock = new InvestimentoModel();
        investimentoMock.setNome("Poupança");
        investimentoMock.setRendimentoMes(0.2);

        simulacaoMock.setInvestimento(investimentoMock);
    }

    @Test
    public void testarBuscarTodasSimulacoes() {

        Iterable<SimulacaoModel> simulacoesIterable = Arrays.asList(simulacaoMock);
        Mockito.when(simulacaoRepository.findAll()).thenReturn(simulacoesIterable);

        List<SimulacaoModel> simulacoesRetorno = simulacaoService.buscarTodasSimulacoes();
        List<SimulacaoModel> simulacoesMock = (List<SimulacaoModel>) simulacoesIterable;

        Assertions.assertArrayEquals(simulacoesMock.toArray(), simulacoesRetorno.toArray());
    }

    @Test
    public void testarCriarSimulacao() {

        Optional<InvestimentoModel> investimentoOptionalMock = Optional.of(investimentoMock);

        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(investimentoOptionalMock);

        ResponseSimulacaoDTO retornoSimulacaoDTO = simulacaoService.criarSimulacao(1, simulacaoMock);

        Assertions.assertEquals(0.2, retornoSimulacaoDTO.getRendimentoPorMes());
        Assertions.assertEquals(5120.0, retornoSimulacaoDTO.getMontante());
    }

}
