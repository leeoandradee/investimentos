package br.com.investimentos.controllers;

import br.com.investimentos.DTOs.ResponseSimulacaoDTO;
import br.com.investimentos.models.InvestimentoModel;
import br.com.investimentos.models.SimulacaoModel;
import br.com.investimentos.services.SimulacaoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;

@WebMvcTest
public class SimulacaoControllerTeste {

    @MockBean
    private SimulacaoService simulacaoService;

    @Autowired
    private MockMvc mockMvc;

    private SimulacaoModel simulacaoMock;

    private InvestimentoModel investimentoMock;

    @BeforeEach
    public void setUp() {

        simulacaoMock = new SimulacaoModel();
        simulacaoMock.setId(1);
        simulacaoMock.setNomeInteressado("Leonardo");
        simulacaoMock.setValorAplicado(5000.0);
        simulacaoMock.setQuantidadeMeses(12);

        investimentoMock = new InvestimentoModel();
        investimentoMock.setNome("Poupança");
        investimentoMock.setRendimentoMes(0.2);

        simulacaoMock.setInvestimento(investimentoMock);
    }

    @Test
    public void testarBuscarTodasSimulacoes() throws Exception {

        List<SimulacaoModel> simulacoesMock = Arrays.asList(simulacaoMock);

        Mockito.when(simulacaoService.buscarTodasSimulacoes()).thenReturn(simulacoesMock);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/simulacoes")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());
    }

    @Test
    public void testarCriarSimulacao() throws Exception {

        ObjectMapper mapper = new ObjectMapper();

        ResponseSimulacaoDTO simulacaoDTOMock  = new ResponseSimulacaoDTO();
        simulacaoDTOMock.setMontante(5120.0);
        simulacaoDTOMock.setRendimentoPorMes(0.2);

        String simulacaoJson = mapper.writeValueAsString(simulacaoDTOMock);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos/1/simulacoes")
                .contentType(MediaType.APPLICATION_JSON).content(simulacaoJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

}
