package br.com.investimentos.controllers;

import br.com.investimentos.models.InvestimentoModel;
import br.com.investimentos.models.SimulacaoModel;
import br.com.investimentos.services.InvestimentoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;

@WebMvcTest(InvestimentoController.class)
public class InvestimentoControllerTeste {

    @MockBean
    private InvestimentoService investimentoService;

    @Autowired
    private MockMvc mockMvc;

    private InvestimentoModel investimentoMock;

    @BeforeEach
    private void setUp() {
        investimentoMock = new InvestimentoModel();
        investimentoMock.setNome("Poupança");
        investimentoMock.setRendimentoMes(0.2);
    }

    @Test
    public void testarCriarInvestimento() throws Exception {
        Mockito.when(investimentoService.criarInvestimento(Mockito.any(InvestimentoModel.class))).thenReturn(investimentoMock);

        ObjectMapper mapper = new ObjectMapper();

        String investimentoJson = mapper.writeValueAsString(investimentoMock);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON).content(investimentoJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Poupança")));
    }

    @Test
    public void testarBuscarTodosInvestimentos() throws Exception {
        List<InvestimentoModel> investimentosMock = Arrays.asList(investimentoMock);

        Mockito.when(investimentoService.buscarTodosInvestimentos()).thenReturn(investimentosMock);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());
    }

}
