package br.com.investimentos.DTOs;

import javax.validation.constraints.Digits;

public class ResponseSimulacaoDTO {

    private double rendimentoPorMes;
    private double montante;

    public ResponseSimulacaoDTO() {
    }

    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }
}
