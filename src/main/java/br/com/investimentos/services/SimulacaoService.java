package br.com.investimentos.services;

import br.com.investimentos.DTOs.ResponseSimulacaoDTO;
import br.com.investimentos.models.InvestimentoModel;
import br.com.investimentos.models.SimulacaoModel;
import br.com.investimentos.repositories.InvestimentoRepository;
import br.com.investimentos.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimulacaoService {

    @Autowired
    SimulacaoRepository simulacaoRepository;

    @Autowired
    InvestimentoService investimentoService;

    public List<SimulacaoModel> buscarTodasSimulacoes() {
        return (List<SimulacaoModel>) simulacaoRepository.findAll();
    }

    public ResponseSimulacaoDTO criarSimulacao(int idInvestimento, SimulacaoModel simulacao) {

        InvestimentoModel investimento = investimentoService.buscarInvestimentoPorId(idInvestimento);

        simulacao.setInvestimento(investimento);
        simulacaoRepository.save(simulacao);

        ResponseSimulacaoDTO simulacaoDTO = new ResponseSimulacaoDTO();
        simulacaoDTO.setRendimentoPorMes(investimento.getRendimentoMes());
        simulacaoDTO.setMontante(calcularMontante(simulacao.getValorAplicado(), investimento.getRendimentoMes(), simulacao.getQuantidadeMeses()));
        return simulacaoDTO;
    }

    public double calcularMontante(double valorInvestimento, double rendimento, int meses) {
        double montante = valorInvestimento;
        montante = montante + (rendimento / 100) * montante * meses;
        return montante;
    }

}
