package br.com.investimentos.services;

import br.com.investimentos.models.InvestimentoModel;
import br.com.investimentos.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    InvestimentoRepository investimentoRepository;

    public InvestimentoModel criarInvestimento(InvestimentoModel investimento) {
        return investimentoRepository.save(investimento);
    }

    public List<InvestimentoModel> buscarTodosInvestimentos() {
        return (List<InvestimentoModel>) investimentoRepository.findAll();
    }

    public InvestimentoModel buscarInvestimentoPorId(int idInvestimento) {
        Optional<InvestimentoModel> investimentoOptional = investimentoRepository.findById(idInvestimento);
        if (investimentoOptional.isPresent()) {
            return investimentoOptional.get();
        } else {
            throw new RuntimeException("ID de investimento não encontrado");
        }
    }


}
