package br.com.investimentos.models;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "investimento")
public class InvestimentoModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "nome do investimento não ser nulo")
    @NotEmpty(message = "nome do investimento não pode ser vazio")
    @Column(unique = true)
    private String nome;

    @NotNull(message = "rendimento ao mes não pode ser nulo")
    private double rendimentoMes;

    public InvestimentoModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoMes() {
        return rendimentoMes;
    }

    public void setRendimentoMes(double rendimentoMes) {
        this.rendimentoMes = rendimentoMes;
    }
}
