package br.com.investimentos.models;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "simulacao")
public class SimulacaoModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "nome não pode ser nulo")
    @NotEmpty(message = "nome do interessado não pode ser vazio")
    private String nomeInteressado;

    @Email
    @NotNull(message = "email não pode ser nulo")
    @NotEmpty(message = "email não pode ser vazio")
    private String email;

    @NotNull(message = "valor aplicado não pode ser nulo")
    private double valorAplicado;

    @NotNull(message = "quantiade de meses não pode ser nulo")
    private int quantidadeMeses;

    @OneToOne
    private InvestimentoModel investimento;

    public SimulacaoModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public InvestimentoModel getInvestimento() {
        return investimento;
    }

    public void setInvestimento(InvestimentoModel investimento) {
        this.investimento = investimento;
    }
}
