package br.com.investimentos.repositories;

import br.com.investimentos.models.SimulacaoModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SimulacaoRepository extends CrudRepository<SimulacaoModel, Integer> {
}
