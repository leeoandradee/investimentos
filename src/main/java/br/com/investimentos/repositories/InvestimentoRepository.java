package br.com.investimentos.repositories;

import br.com.investimentos.models.InvestimentoModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvestimentoRepository  extends CrudRepository<InvestimentoModel, Integer> {
}
