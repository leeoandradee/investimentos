package br.com.investimentos.controllers;

import br.com.investimentos.DTOs.ResponseSimulacaoDTO;
import br.com.investimentos.models.SimulacaoModel;
import br.com.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/investimentos")
public class SimulacaoController {

    @Autowired
    SimulacaoService simulacaoService;

    @GetMapping("simulacoes")
    public List<SimulacaoModel> buscarTodasSimulacoes() {
        return simulacaoService.buscarTodasSimulacoes();
    }

    @PostMapping("/{id}/simulacoes")
    public ResponseSimulacaoDTO criarSimulacao(@PathVariable(name = "id") int idInvestimento, @RequestBody SimulacaoModel simulacao) {
        try {
            return simulacaoService.criarSimulacao(idInvestimento, simulacao);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}