package br.com.investimentos.controllers;

import br.com.investimentos.models.InvestimentoModel;
import br.com.investimentos.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    InvestimentoService investimentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public InvestimentoModel criarInvestimento(@RequestBody @Valid InvestimentoModel investimento) {
        try {
            return investimentoService.criarInvestimento(investimento);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping
    public List<InvestimentoModel> buscarTodosInvestimentos() {
        return investimentoService.buscarTodosInvestimentos();
    }

}
